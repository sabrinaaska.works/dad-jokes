package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/tentangkode/go-restapi-fiber/controllers/memecontroller"
)

func main() {
	app := fiber.New()

	// /api/memes...

	api := app.Group("/api")
	meme := api.Group("/memes")

	meme.Get("/", memecontroller.Show)

	app.Listen(":8000")
}