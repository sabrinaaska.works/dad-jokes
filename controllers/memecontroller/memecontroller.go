package memecontroller

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/tentangkode/go-restapi-fiber/initial"
)

func Show(c *fiber.Ctx) error {
	client := &http.Client{}
	var errorMessage string = "null"
	var value string
	req, err := http.NewRequest("GET", "https://icanhazdadjoke.com/", nil)

	if err != nil{
		errorMessage = err.Error()
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		errorMessage = err.Error()
	}

	defer res.Body.Close()
	bodyBytes, errBody := ioutil.ReadAll(res.Body)
	if errBody != nil {
		value = errBody.Error()
	}

	var responseObject initial.Meme
 	json.Unmarshal(bodyBytes, &responseObject)

	status := responseObject.Status
	value = responseObject.Joke

	return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
		"status": status,
		"jokes": value,
		"error": errorMessage,
	})
}
